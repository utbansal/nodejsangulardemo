var delegate = require('../server/services');

exports.service = function(req, res) {
	var query = req.query;
	if (query.serviceName) {
		if (query.serviceName === 'sales') {
			var result = delegate.getSalesOrders(query, function(result) {
				res.json(result);
				return;
			});
		}
	} else {
		res.json("Error in request: parameter missing");
		return;
	}

}

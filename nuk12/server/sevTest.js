function SevTest (){

testDBConnection: function() {

	connection.connect();

	connection.query('SELECT * from sales_order', function(err, rows, fields) {
		if (!err)
			console.log('The solution is: ', rows);
		else
			console.log('Error while performing Query.');
	});

	connection.end();

}

testDBPool : function (app,pool) {

	pool.getConnection(function(err, connection) {
		if (err) {
			connection.release();
			res.json({
				"code" : 100,
				"status" : "Error in connection database"
			});
			return;
		}

		console.log('connected as id ' + connection.threadId);

		connection.query("select * from user", function(err, rows) {
			connection.release();
			if (!err) {
				res.json(rows);
			}
		});

		connection.on('error', function(err) {
			res.json({
				"code" : 100,
				"status" : "Error in connection database"
			});
			return;
		});
	});
}
}();



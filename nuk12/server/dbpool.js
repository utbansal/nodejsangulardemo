var mysql = require('mysql');
var dbpool = mysql.createPool({
	connectionLimit : 10,
	host : 'localhost',
	user : 'root',
	password : 'mysql',
	database : 'barcelona_mm',
	debug : false
});


module.exports = dbpool;
var dbpool = require('./dbpool');
exports.getSalesOrders = function getSalesOrders(query, resultCallback) {
	dbpool.getConnection(function(err, connection) {
		if (!err) {
			console.log('connected as id ' + connection.threadId);
			connection.query("select * from sales_order", function(err, rows) {
				connection.release();
				if (!err) {
					resultCallback(rows);
				}
			});
		}

	});
};
